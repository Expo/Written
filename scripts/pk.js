// Updates all package.json's
import fs from 'fs';
import path from 'path';

const searchDirs = ['packages'];

const basePkg = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
basePkg.scripts = {
  build: 'tsc',
  dev: 'nodemon --watch src -e ts,json,env --delay 50ms --exec "npm run build"',
};
basePkg.dependencies = {};
basePkg.devDependencies = Object.fromEntries(
  Object.entries(basePkg.devDependencies).filter(([k]) =>
    ['concurrently', 'nodemon', 'esbuild', '@3xpo/es-many'].includes(k),
  ),
);
basePkg.peerDependencies = {};

// remove empty fields
delete basePkg.dependencies;
delete basePkg.peerDependencies;

const copyFields = [
  // 'version',
  'author',
  'repository',
  'homepage',
  'bugs',
  'license',
  'engines',
  'type',
];
const copyFieldsIfNull = [
  'name',
  'description',
  'scripts',
  'contributors',
  'maintainers',
  'version',
  'dependencies',
  'devDependencies',
  'files',
];

const recReaddirSync = dir => {
  if (dir.endsWith('node_modules')) return [];
  return fs
    .readdirSync(dir)
    .map(f => dir + '/' + f)
    .flatMap(v => (fs.statSync(v).isDirectory() ? recReaddirSync(v) : v));
};

const pkgs = searchDirs
  .map(dir =>
    recReaddirSync(dir).filter(
      v => v.endsWith('package.json') && !v.includes('/node_modules/'),
    ),
  )
  .flat();

pkgs.forEach(pkgPath => {
  console.log(`Updating Package: ${pkgPath}`);

  // Get the file
  const pkgFile = JSON.parse(fs.readFileSync(pkgPath, 'utf-8'));
  // Patch the file
  Object.entries(basePkg).forEach(([k, v]) => {
    if (copyFields.includes(k)) pkgFile[k] = v;
    if (
      copyFieldsIfNull.includes(k) &&
      (typeof pkgFile[k] === 'undefined' || pkgFile[k] === null)
    )
      pkgFile[k] = v;
    if (k === 'repository' && pkgFile[k]) {
      pkgFile[k].directory = pkgPath.replace(/package.json/giu, '');
    } else if (k === 'homepage') {
      pkgFile.homepage = `${v}/${
        v.includes('github.com') ? 'blob' : 'src/branch'
      }/senpai/${pkgPath.replace(/package.json/giu, '')}`;
    }
  });

  // Ensure devDeps is at the bottom
  const devDeps = pkgFile.devDependencies;
  delete pkgFile.devDependencies;
  pkgFile.devDependencies = devDeps;

  // Write
  fs.writeFileSync(pkgPath, JSON.stringify(pkgFile, null, 2) + '\n');

  // Copy LICENSE file
  const licenseFile = 'LICENSE';
  const licensePath = path.join(path.dirname(pkgPath), licenseFile);
  if (!fs.existsSync(licensePath)) fs.copyFileSync(licenseFile, licensePath);
});
