<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @awfn/define-handler

Simple wrapper around appwrite-types for making stanadlone functions be just the tiniest bit smaller.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@awfn/define-handler)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your function, run:

```sh
pnpm i @awfn/define-handler
```

## Usage

```ts
import { defineHandler } from '@awfn/define-handler';

export default defineHandler(async (req, res) => {
  // req & res are now fully typed
  res.send('Hello, world!');
});
```
