import {
  AppwriteResponseObject,
  AppwriteReq,
  AppwriteRequestHeaders,
} from '@written/appwrite-types';
export class HttpCompat {
  /** Note: this is essentially guessing */
  public static bufferIsBinary = (
    arrayBuffer: ArrayLike<number> | ArrayBufferLike,
  ): boolean => {
    const uint8Array = new Uint8Array(arrayBuffer);

    for (const byte of uint8Array)
      if (byte === 0x00 || (byte < 0x20 && byte !== 0x0a && byte !== 0x0d))
        return true;

    return false;
  };
  protected static tryJSONParse<T = string>(v: string, fallback = v as T) {
    try {
      return JSON.parse(v);
    } catch (e) {
      return fallback;
    }
  }
  public static appwriteRequestToHttpRequest(
    appwriteRequest: AppwriteReq,
  ): Request {
    return new Request(appwriteRequest.url, {
      body: appwriteRequest.bodyRaw,
      headers: appwriteRequest.headers,
      method: appwriteRequest.method,
    });
  }
  public static appwriteResponseToHttpResponse(
    appwriteResponse: AppwriteResponseObject,
  ): Response {
    return new Response(
      appwriteResponse
        ? typeof appwriteResponse.body === 'string'
          ? appwriteResponse.body
          : JSON.stringify(appwriteResponse.body)
        : '',
      {
        headers: appwriteResponse.headers ?? {},
        status: appwriteResponse.statusCode,
      },
    );
  }
  public static async httpRequestToAppwriteRequest(
    req: Request,
    allowBinaryBody = true,
  ): Promise<AppwriteReq> {
    let headers = {} as AppwriteRequestHeaders;
    req.headers.forEach((v, k) => (headers[k] = v));
    req.headers.forEach((v, k) => (headers[k.toLowerCase()] = v));
    headers = await AppwriteRequestHeaders.parseAsync(headers);
    const bodyRawAsBuffer = await req.arrayBuffer();
    const text = new TextDecoder().decode(bodyRawAsBuffer);
    const url = new URL(req.url);
    const query = {} as Record<string, string>;
    url.searchParams.forEach((v, k) => (query[k] = v));
    return {
      headers,
      bodyRaw: text,
      url: req.url,
      host: url.host ?? url.hostname,
      method: req.method,
      path: url.pathname,
      port: Number(url.port || '80'),
      queryString: url.search,
      query,
      scheme: url.protocol.replace(':', '') as AppwriteReq['scheme'],
      body: headers['content-type']?.toLowerCase()?.includes('application/json')
        ? this.tryJSONParse(
            text,
            allowBinaryBody && this.bufferIsBinary(bodyRawAsBuffer)
              ? bodyRawAsBuffer
              : text,
          )
        : allowBinaryBody && this.bufferIsBinary(bodyRawAsBuffer)
          ? bodyRawAsBuffer
          : text,
    };
  }
  public static async httpResponseToAppwriteResponse<
    IsArrayBuffer = true | false | 'auto',
  >(
    res: Response,
    /** Note: Binary='auto' is essentially guessing. */
    asBinary = 'auto' as IsArrayBuffer,
  ) {
    const headers = {} as Record<string, string>;
    res.headers.forEach((v, k) => (headers[k] = v));
    let buffer = asBinary ? await res.arrayBuffer() : await res.text();
    if (asBinary === 'auto' && !this.bufferIsBinary(buffer as ArrayBuffer))
      buffer = new TextDecoder().decode(buffer as ArrayBuffer);
    return {
      headers,
      statusCode: res.status,
      body: buffer,
    } satisfies AppwriteResponseObject as {
      headers: typeof headers;
      statusCode: typeof res.status;
      body: IsArrayBuffer extends 'auto'
        ? ArrayBuffer | string
        : IsArrayBuffer extends true
          ? ArrayBuffer
          : string;
    };
  }
}
export default HttpCompat;
