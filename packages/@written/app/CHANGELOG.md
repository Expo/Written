# @written/app

## 0.5.4

### Patch Changes

- a748109: Update Deps
- Updated dependencies [a748109]
  - @written/route-parameters@0.1.11
  - @written/appwrite-types@0.2.17
  - @written/trailing-slash@0.1.16
  - @written/cors-setup@0.3.18
  - @written/wrap@1.0.18

## 0.5.3

### Patch Changes

- 51d9506: Upgrade Dependencies
- Updated dependencies [51d9506]
  - @written/route-parameters@0.1.10
  - @written/appwrite-types@0.2.16
  - @written/trailing-slash@0.1.15
  - @written/cors-setup@0.3.17
  - @written/wrap@1.0.17

## 0.5.2

### Patch Changes

- d1ea241: Update Deps
- Updated dependencies [d1ea241]
  - @written/route-parameters@0.1.9
  - @written/appwrite-types@0.2.15
  - @written/trailing-slash@0.1.14
  - @written/cors-setup@0.3.16
  - @written/wrap@1.0.16

## 0.5.1

### Patch Changes

- Updated dependencies [1df6d84]
  - @written/appwrite-types@0.2.14
  - @written/cors-setup@0.3.15
  - @written/wrap@1.0.15

## 0.5.0

### Minor Changes

- fbd6360: Use the AppwriteRequestHeaders object

### Patch Changes

- 4fa1758: Upgrade dependencise
- a0c9930: Upgrade Dependencies
- Updated dependencies [034c27f]
- Updated dependencies [4fa1758]
- Updated dependencies [39ce77a]
- Updated dependencies [a0c9930]
  - @written/appwrite-types@0.2.13
  - @written/route-parameters@0.1.8
  - @written/trailing-slash@0.1.13
  - @written/cors-setup@0.3.14
  - @written/wrap@1.0.14

## 0.4.9

### Patch Changes

- Updated dependencies [816cec3]
  - @written/appwrite-types@0.2.12
  - @written/cors-setup@0.3.13
  - @written/wrap@1.0.13

## 0.4.8

### Patch Changes

- 8acd2e0: Update License Year
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format
- Updated dependencies [3872f26]
- Updated dependencies [8acd2e0]
- Updated dependencies [bee1c23]
- Updated dependencies [de25848]
  - @written/route-parameters@0.1.7
  - @written/trailing-slash@0.1.12
  - @written/appwrite-types@0.2.11
  - @written/cors-setup@0.3.12
  - @written/wrap@1.0.12

## 0.4.7

### Patch Changes

- 40f46fa: Upgrade Dependencies
- Updated dependencies [40f46fa]
  - @written/route-parameters@0.1.6
  - @written/appwrite-types@0.2.10
  - @written/trailing-slash@0.1.11
  - @written/cors-setup@0.3.11
  - @written/wrap@1.0.11

## 0.4.6

### Patch Changes

- 4a5dd4d: bump dependencies
- Updated dependencies [4a5dd4d]
  - @written/route-parameters@0.1.5
  - @written/appwrite-types@0.2.9
  - @written/trailing-slash@0.1.10
  - @written/cors-setup@0.3.10
  - @written/wrap@1.0.10

## 0.4.5

### Patch Changes

- rebuild everything due to a filesystem error during the last build
- Updated dependencies
  - @written/appwrite-types@0.2.8
  - @written/cors-setup@0.3.9
  - @written/route-parameters@0.1.4
  - @written/trailing-slash@0.1.9
  - @written/wrap@1.0.9

## 0.4.4

### Patch Changes

- cc4fcee: Use the ~ version range
- Updated dependencies [cc4fcee]
  - @written/cors-setup@0.3.8
  - @written/wrap@1.0.8

## 0.4.3

### Patch Changes

- bump everything
- Updated dependencies
  - @written/appwrite-types@0.2.7
  - @written/cors-setup@0.3.7
  - @written/route-parameters@0.1.3
  - @written/trailing-slash@0.1.8
  - @written/wrap@1.0.7

## 0.4.2

### Patch Changes

- 18c6dd1: fix: use es6 in lib, i hate you typescript
- Updated dependencies [dd14f4c]
- Updated dependencies [d3d5f04]
- Updated dependencies [18c6dd1]
  - @written/trailing-slash@0.1.7
  - @written/cors-setup@0.3.6
  - @written/route-parameters@0.1.2
  - @written/appwrite-types@0.2.6
  - @written/wrap@1.0.6

## 0.4.1

### Patch Changes

- Updated dependencies [0e09226]
- Updated dependencies [48bc017]
  - @written/trailing-slash@0.1.6
  - @written/route-parameters@0.1.1

## 0.4.0

### Minor Changes

- 78b268e: Change generifs of all app. methods, this is breaking!
- 3ae4ab7: Refactor the per-method shitfuckery

### Patch Changes

- 48692e6: Extract Request type, we now have a req.ts
- 4301ecc: Add req.get()/req.getHeader()/req.header()
- 48692e6: Minor internal type changes
- 1208d0d: Strict Mode, for everything
- Updated dependencies [1208d0d]
  - @written/appwrite-types@0.2.5
  - @written/trailing-slash@0.1.5
  - @written/cors-setup@0.3.5
  - @written/wrap@1.0.5

## 0.3.4

### Patch Changes

- Updated dependencies [bca8b56]
  - @written/appwrite-types@0.2.4
  - @written/cors-setup@0.3.4
  - @written/trailing-slash@0.1.4
  - @written/wrap@1.0.4

## 0.3.3

### Patch Changes

- 825ec04: properly add a ? for query

## 0.3.2

### Patch Changes

- Updated dependencies [06a407b]
  - @written/trailing-slash@0.1.3

## 0.3.1

### Patch Changes

- Updated dependencies [3101689]
  - @written/trailing-slash@0.1.2

## 0.3.0

### Minor Changes

- b4705dc: Add Trailing Slash Stripping

## 0.2.3

### Patch Changes

- c09e234: Add LICENSE to npm files
- Updated dependencies [c09e234]
  - @written/appwrite-types@0.2.3
  - @written/cors-setup@0.3.3
  - @written/wrap@1.0.3

## 0.2.2

### Patch Changes

- 9af1ae7: Mark as side-effect-free
- Updated dependencies [9af1ae7]
- Updated dependencies [b7955f5]
  - @written/appwrite-types@0.2.2
  - @written/cors-setup@0.3.2
  - @written/wrap@1.0.2

## 0.2.1

### Patch Changes

- Bump Version
- Updated dependencies
  - @written/appwrite-types@0.2.1
  - @written/cors-setup@0.3.1
  - @written/wrap@1.0.1

## 0.2.0

### Minor Changes

- ad89eae: Migrate to nx

### Patch Changes

- Updated dependencies [ad89eae]
- Updated dependencies [ad89eae]
  - @written/appwrite-types@0.2.0
  - @written/cors-setup@0.3.0
  - @written/wrap@1.0.0

## 0.1.6

### Patch Changes

- Updated dependencies [e1c8c20]
  - @written/cors-setup@0.2.2
  - @written/wrap@0.1.6
  - @written/appwrite-types@0.1.6

## 0.1.5

### Patch Changes

- 8917383: Add Exports Object to Package
- Updated dependencies [8917383]
  - @written/cors-setup@0.2.1
  - @written/wrap@0.1.5
  - @written/appwrite-types@0.1.5

## 0.1.4

### Patch Changes

- 9e8df2e: Add README to @written/app
- Updated dependencies [202b68e]
- Updated dependencies [d296d3c]
- Updated dependencies [3cc92a2]
- Updated dependencies [96f55e2]
  - @written/wrap@0.1.4
  - @written/cors-setup@0.2.0
  - @written/appwrite-types@0.1.4

## 0.1.3

### Patch Changes

- Updated dependencies [d23e286]
  - @written/appwrite-types@0.1.3
  - @written/cors-setup@0.1.3
  - @written/wrap@0.1.3

## 0.1.2

### Patch Changes

- cbe6bed: Make homepage be the specific project repo dir
- Updated dependencies [cbe6bed]
  - @written/appwrite-types@0.1.2
  - @written/cors-setup@0.1.2
  - @written/wrap@0.1.2

## 0.1.1

### Patch Changes

- 50e20ad: Initial Changeset
- Updated dependencies [50e20ad]
  - @written/appwrite-types@0.1.1
  - @written/cors-setup@0.1.1
  - @written/wrap@0.1.1
