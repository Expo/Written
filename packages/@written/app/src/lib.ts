import type {
  AppwriteContext,
  AppwriteDefault as AppwriteServerFunction,
  AppwriteResponseObject,
} from '@written/appwrite-types';
import wrap from '@written/wrap';
import {
  OptionsObject as CorsOptionsObject,
  wrapCorsResponse,
} from '@written/cors-setup';
import Route from 'route-parser';
import { z } from 'zod';
import { Trailing, TrailingSlashes } from '@written/trailing-slash';
import { Request, ParamCtx, NextFunc } from './req';
import { ParamsDictionary, RouteParameters } from '@written/route-parameters';

/** Function for registering middleware */
type RouterMiddlewareRegistrationFunc<ThisType> = (<
  Route extends string | [...string[]] | ParamsDictionary | null,
>(
  path: Route extends string | null ? Route : string,
  callback: MiddlewareFunction<
    Route extends string
      ? RouteParameters<Route>
      : Route extends [...string[]]
        ? {
            [k in Route[number]]: string;
          }
        : Route extends ParamsDictionary
          ? Route
          : {}
  >,
) => ThisType) &
  ((callback: MiddlewareFunction, _?: undefined) => ThisType);

/** Middleware Callback Function */
export type MiddlewareFunction<Params extends Record<string, string> = {}> = (
  ctx: ParamCtx<Params>,
  next: NextFunc,
) => AppwriteResponseObject | Promise<AppwriteResponseObject>;

export enum MethodEnum {
  /** Only use this if it's the only object | will match all */
  // @ts-ignore
  Any = null,
  Get = 'GET',
  Head = 'HEAD',
  Post = 'POST',
  Put = 'PUT',
  Delete = 'DELETE',
  Connect = 'CONNECT',
  Options = 'OPTIONS',
  Trace = 'TRACE',
  Patch = 'PATCH',
}
export const ZMethod = z.nativeEnum(MethodEnum);
export const Method = ZMethod.enum;
export type Method = z.infer<typeof ZMethod>;

/** Internal Representation of all middleware */
export class Middleware<
  Params extends Record<string, string> = {},
  Methods extends Method[] = [typeof Method.Any],
> {
  public constructor(
    public methods: Methods,
    public callback: MiddlewareFunction<Params> = ctx =>
      ctx.res.json({
        success: false,
        error: '500: No Callback Implemented in Middleware',
        code: 'EDEFAULTCALLBACK',
      }),
  ) {}
  public route: Route<Params> | undefined = undefined;
  public isUnrouted = false;
}

/** Modes the server can run in - could, in future, be used to provide non-appwrite support */
export enum ServerMode {
  Appwrite,
}

export class App {
  public readonly config = {
    /** Internal option in regards to if we should pass trailing slashes to routing or not */
    routingTrailingSlashes: TrailingSlashes.Never,
  };
  public set<T extends keyof typeof this.config>(
    key: T,
    value: (typeof this.config)[T],
  ) {
    this.config[key] = value;
    return this;
  }
  public notFound = (ctx: AppwriteContext) => {
    if (
      'locals' in ctx &&
      typeof ctx.locals === 'object' &&
      ctx.locals !== null
    )
      (ctx.locals as Record<string, any>).is404 = true;
    return ctx.req.method.toUpperCase() === 'OPTIONS'
      ? ctx.res.send('', 204)
      : ctx.res.json(
          {
            success: false,
            error: '404: Not Found',
            code: 'ENOENT',
          },
          404,
        );
  };
  public middlewares: Middleware<any, any>[] = [];
  /**
   * @param func The middleware
   * @param path The path to match
   */
  protected createMiddleware<
    Methods extends Method[] = [typeof Method.Any],
    Params extends Record<string, string> = {},
  >(func: MiddlewareFunction<Params>, path: null | string, methods: Methods) {
    if (!path?.startsWith('/')) path = null;
    const stripped = path
      ? Trailing.strip(
          path,
          this.config.routingTrailingSlashes ?? TrailingSlashes.Never,
        )
      : void 0;
    const route = stripped ? new Route<Params>(stripped) : undefined;
    const middleware = new Middleware<Params, Methods>(methods);
    middleware.callback = func;
    if (path !== null) middleware.route = route;
    else middleware.isUnrouted = true;
    this.middlewares.push(middleware);
    return this;
  }
  public method(...methods: Method[]): RouterMiddlewareRegistrationFunc<this> {
    return ((
      arg1: Parameters<RouterMiddlewareRegistrationFunc<this>>[0],
      arg2?: Parameters<RouterMiddlewareRegistrationFunc<this>>[1],
    ) => {
      let path = null as null | string;
      let func = null as unknown as MiddlewareFunction<
        Route extends string ? RouteParameters<Route> : {}
      >;
      if (typeof arg1 === 'function') {
        func = arg1;
      } else if (typeof arg2 === 'function') {
        func = arg2;
      }
      if (typeof arg1 === 'string') path = arg1;
      return this.createMiddleware(func, path, methods);
    }) as RouterMiddlewareRegistrationFunc<this>;
  }
  public get = this.method(Method.Get);
  public post = this.method(Method.Post);
  public put = this.method(Method.Put);
  public delete = this.method(Method.Delete);
  public connect = this.method(Method.Connect);
  public trace = this.method(Method.Trace);
  public patch = this.method(Method.Patch);
  public options = this.method(Method.Options);
  public use = this.method(Method.Any);
  protected appwriteExec(ctx: AppwriteContext) {
    const strip = Trailing.getTrailingSlashStrippingFunction(
      this.config.routingTrailingSlashes ?? TrailingSlashes.Never,
    );

    let idx = 0;
    const locals = {},
      method = ctx.req.method.toUpperCase(),
      rawPath =
        ctx.req.path +
          (ctx.req.queryString
            ? ctx.req.queryString.startsWith('?')
              ? ''
              : '?'
            : '') +
          ctx.req.queryString ?? '',
      path = strip(rawPath);

    const getNextCaller = () => {
      const ln = this.middlewares.length;
      while (idx <= ln) {
        const middleware = this.middlewares[idx++];
        if (middleware) {
          if (
            middleware.methods.includes(Method.Any) ||
            middleware.methods.includes(method)
          ) {
            const match =
              middleware.isUnrouted || !middleware.route
                ? {}
                : middleware.route.match(path);
            if (match !== false)
              return {
                callback: middleware.callback,
                middleware: middleware as Middleware<typeof match>,
                params: match,
                is404: false as const,
              } as const;
          }
        }
      }
      return {
        callback: (ctx => this.notFound(ctx)) as MiddlewareFunction,
        middleware: null as null,
        params: {} as const,
        is404: true as const,
      } as const;
    };
    const next = (() => {
      const callerInfo = getNextCaller();
      req.params = callerInfo.params;
      return callerInfo.callback(
        {
          ...ctx,
          params: callerInfo.params,
          next,
          locals,
          req: req as any,
        },
        next,
      );
    }) as NextFunc;

    const req = new Request({
      ...ctx.req,
      next,
    });
    return next();
  }
  public cors(options?: CorsOptionsObject) {
    const wrapper = wrap((ctx, rs) => wrapCorsResponse(ctx, rs, options));
    this.use(
      wrapper(ctx =>
        'next' in ctx
          ? (ctx.next as NextFunc)()
          : ctx.res.json({
              success: false,
              error: 'Did not have next in ctx',
              code: 'ENOCTX',
            }),
      ),
    );
    return this;
  }
  /** Returns middleware for adding/removing trailing slashes from ctx */
  public static trailingSlash(mode: TrailingSlashes): MiddlewareFunction {
    const strip = Trailing.getTrailingSlashStrippingFunction(mode);
    return ctx => {
      ctx.req.path = strip(ctx.req.path);
      return ctx.next();
    };
  }
  /** Adds middleware determining whether to use trailing slashes */
  public trailingSlash(mode: TrailingSlashes) {
    return this.use(App.trailingSlash(mode));
  }
  /** Returns a server */
  public server<Mode extends ServerMode = ServerMode.Appwrite>(
    mode?: Mode,
  ): Mode extends ServerMode.Appwrite ? AppwriteServerFunction : never {
    return this._server(mode ?? ServerMode.Appwrite) as any; // wrap, as i dont want to deal with return type missmatching due to weird shenanigans
  }
  private _server(mode: ServerMode) {
    const instance = this;
    switch (mode) {
      case ServerMode.Appwrite:
        return (ctx: AppwriteContext) => instance.appwriteExec(ctx);

      default:
        throw new Error(`Unsupported Server Mode: ${mode}`);
    }
  }
}
export { App as default, Request, ParamCtx, NextFunc };
