import mime from 'mime-types';
import {
  AppwriteContext,
  AppwriteResponseObject,
} from '@written/appwrite-types';
import { access, constants, readFile, stat } from 'fs/promises';
import path from 'path';
import { type ZodString, z } from 'zod';
import { MiddlewareFunction } from '@written/app';
export type Options = z.infer<typeof Options>;
const Options = z.object({
  /** Absolute path of the file on the filesystem */
  directory: z
    .string()
    .startsWith(
      '/',
      "Must be an absolute filepath - use import.meta.dirname or __dirname to resolve the directory of the function's current JS binary",
    ),
  /** Base Route - e.g. `/a/b` if you want `/a/b/c` to resolve to `{directory}/c`. Any paths outside of this will not resolve */
  baseRoute: z
    .string()
    .startsWith('/', 'Must be an absolute route')
    .default('/')
    .optional(),
  /** Cache Control - defaults to 3 days */
  cacheControl: z.string().default('public, max-age=259200').optional(),
  /** Disallowed Paths - If these are found anywhere in a path, the request will be denied */
  disallowedPaths: z
    .array(z.string())
    .default(['private', '.svelte-kit', '.env'])
    .optional(),
  /** Index file */
  index: z.string().default('index.html').optional(),
  /** Default Fallback File, returned on 404's */
  notFoundFile: z.string().default('404.html').optional(),
  /** Content Type Resolution Function - Null = use file extension based content type resolution */
  contentTypeResolver: z
    .function(
      z.tuple([z.string()] as [filepath: ZodString]),
      z.string().or(z.null()),
    )
    .default(() => null)
    .optional(),
});
/** Raw Appwrite Static Function; returns null on 404. */
export const appwriteStatic = (
  _options: Options,
): ((ctx: AppwriteContext) => Promise<AppwriteResponseObject | null>) => {
  const options = Options.parse(_options);
  let notFoundFile: Buffer | null = null;
  let notFoundFileType = 'text/plain';
  const is404Reachable = (async () => {
    const filePath = path.resolve(
      options.directory,
      options.notFoundFile ?? '404.html',
    );
    try {
      await access(filePath, constants.F_OK);
      notFoundFile = await readFile(filePath);
      notFoundFileType =
        options.contentTypeResolver?.(filePath) ??
        (mime.lookup(filePath) || notFoundFileType);
      return true;
    } catch (error) {
      return false;
    }
  })();
  return async ({ req, res }) => {
    const route = path.relative(options.baseRoute ?? '/', req.path);
    if (route.startsWith('..'))
      return res.json(
        {
          success: false,
          error: 'Access Denied',
          detail: `Cannot access parent path in this function - the baseRoute passed was ${options.baseRoute}, which when resolved with ${req.path} (your path), results in ${route}, which lives in a parent directory.`,
        },
        403,
      );
    if (path.isAbsolute(route))
      return res.json(
        {
          success: false,
          error: 'Access Denied',
          detail: `path.relative() returned absolute path.`,
        },
        403,
      );
    if (
      (options.disallowedPaths ?? ['private', '.svelte-kit', '.env']).find(v =>
        route.includes(v),
      )
    )
      return res.json(
        {
          success: false,
          error: 'Access Denied',
          detail: `Path includes disallowed subpath.`,
        },
        403,
      );
    const absolute = path.resolve(options.directory, route);
    let absPath = absolute;

    try {
      await access(absPath, constants.F_OK);
      if ((await stat(absPath)).isDirectory())
        absPath = path.join(absPath, options.index ?? 'index.html');
      const fileContent = await readFile(absPath);
      const mimeType =
        options.contentTypeResolver?.(absPath) ??
        (mime.lookup(absPath) || 'text/plain');
      return res.send(fileContent, 200, {
        'cache-control': options.cacheControl ?? 'public, max-age=259200',
        ...(mimeType
          ? {
              'content-type': mimeType,
            }
          : {}),
      });
    } catch (error) {
      if (await is404Reachable)
        return res.send(notFoundFile, 404, {
          ...(notFoundFileType
            ? {
                'content-type': notFoundFileType,
              }
            : {}),
        });
      else return null;
    }
  };
};
export const writtenStatic = (options: Options) => {
  const middleware = appwriteStatic(options);
  return (async (ctx, nx) => {
    return (await middleware(ctx)) ?? nx();
  }) satisfies MiddlewareFunction;
};
export default writtenStatic;
