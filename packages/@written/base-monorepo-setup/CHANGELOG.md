# @written/base-monorepo-setup

## 0.1.13

### Patch Changes

- a748109: Update Deps

## 0.1.12

### Patch Changes

- 51d9506: Upgrade Dependencies

## 0.1.11

### Patch Changes

- d1ea241: Update Deps

## 0.1.10

### Patch Changes

- 4fa1758: Upgrade dependencise
- a0c9930: Upgrade Dependencies

## 0.1.9

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format

## 0.1.8

### Patch Changes

- 40f46fa: Upgrade Dependencies

## 0.1.7

### Patch Changes

- 4a5dd4d: bump dependencies

## 0.1.6

### Patch Changes

- rebuild everything due to a filesystem error during the last build

## 0.1.5

### Patch Changes

- f14b10d: Create a .gitignore file

## 0.1.4

### Patch Changes

- bump everything

## 0.1.3

### Patch Changes

- dd14f4c: use @3xpo/fs-extra
- bd7aa58: Upgrade Dependencies
- 18c6dd1: fix: use es6 in lib, i hate you typescript

## 0.1.2

### Patch Changes

- af783d9: add fs-extra to deps

## 0.1.1

### Patch Changes

- 48bc017: Upgrade Dependencies
