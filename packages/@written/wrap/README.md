<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/wrap

Create a wrapper (primitive "middleware") around an Appwrite Function. It's recommended to just use `@written/app`.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/trailing-slash)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

```sh
pnpm i @written/wrap @written/appwrite-types
```

## Usage

```ts
// wrapper.ts
import wrap from '@written/wrap';
export default wrap((ctx, rs) => {
  rs.headers['x-abc'] = ctx.req.headers['x-abc'] ?? '';
  return rs;
});
```

This example middleware will replace the `x-abc` header in the response with the same header as found in the request.

You can then use it as such:

```ts
import w from './wrapper';
export default w(ctx => {
  return ctx.res.json({
    success: 1,
    message: 'Hi!',
  });
});
```
