<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# @written/trailing-slash

Trim/Enforce Trailing Slashes - A Utility Normalization Library.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/@written/trailing-slash)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Installation

In your project, run:

```sh
pnpm i @written/trailing-slash
```

## Usage

```ts
import Trailing from '@written/trailing-slash';

Trailing.process('/a/b/c', Trailing.Mode.Never); // -> /a/b/c
Trailing.process('/a/b/c/', Trailing.Mode.Never); // -> /a/b/c
Trailing.process('/a/b/c', Trailing.Mode.Always); // -> /a/b/c/
Trailing.process('/a/b/c/', Trailing.Mode.Always); // -> /a/b/c/
Trailing.process('/a/b/c', Trailing.Mode.Ignore); // -> /a/b/c
Trailing.process('/a/b/c/', Trailing.Mode.Ignore); // -> /a/b/c/
```

We always keep the trailing slash if it's the only item in the path; e.g.

```ts
Trailing.process('/', Trailing.Mode.Never); // -> /
Trailing.process('/', Trailing.Mode.Always); // -> /
Trailing.process('/', Trailing.Mode.Ignore); // -> /

Trailing.process('/?a', Trailing.Mode.Never); // -> /?a
Trailing.process('/?a', Trailing.Mode.Always); // -> /?a
Trailing.process('/?a', Trailing.Mode.Ignore); // -> /?a
```

### Shorthands

You can also use `Trailing.strip(str)` as a shorthand to `Trailing.process(str,Trailing.Mode.Never)`, and `Trailing.enforce(str)` as a shorthand to `Trailing.process(str,Trailing.Mode.Always)`.

You can also get the function that internall strips a string directly by calling `Trailing.getTrailingSlashStrippingFunction(mode)` - this will return a function that takes & returns a string.
