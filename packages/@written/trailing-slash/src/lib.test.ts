import Trailing from './lib';

describe('Trailing.process', () => {
  it('should trim trailing slash when mode is Never', () => {
    expect(Trailing.process('/a/b/c', Trailing.Mode.Never)).toBe('/a/b/c');
    expect(Trailing.process('/a/b/c/', Trailing.Mode.Never)).toBe('/a/b/c');
  });

  it('should enforce trailing slash when mode is Always', () => {
    expect(Trailing.process('/a/b/c', Trailing.Mode.Always)).toBe('/a/b/c/');
    expect(Trailing.process('/a/b/c/', Trailing.Mode.Always)).toBe('/a/b/c/');
  });

  it('should handle trailing slash according to the path when mode is Ignore', () => {
    expect(Trailing.process('/a/b/c', Trailing.Mode.Ignore)).toBe('/a/b/c');
    expect(Trailing.process('/a/b/c/', Trailing.Mode.Ignore)).toBe('/a/b/c/');
  });

  it("should keep trailing slash if it's the only item in the path", () => {
    expect(Trailing.process('/', Trailing.Mode.Never)).toBe('/');
    expect(Trailing.process('/', Trailing.Mode.Always)).toBe('/');
    expect(Trailing.process('/', Trailing.Mode.Ignore)).toBe('/');

    expect(Trailing.process('/?a', Trailing.Mode.Never)).toBe('/?a');
    expect(Trailing.process('/?a', Trailing.Mode.Always)).toBe('/?a');
    expect(Trailing.process('/?a', Trailing.Mode.Ignore)).toBe('/?a');
  });

  it('should handle query parameters correctly', () => {
    expect(Trailing.process('/a?b', Trailing.Mode.Never)).toBe('/a?b');
    expect(Trailing.process('/a/?b', Trailing.Mode.Never)).toBe('/a?b');
    expect(Trailing.process('/a?b', Trailing.Mode.Always)).toBe('/a/?b');
    expect(Trailing.process('/a/?b', Trailing.Mode.Always)).toBe('/a/?b');
  });
});
