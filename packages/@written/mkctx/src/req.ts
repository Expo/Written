import { AppwriteReq } from '@written/appwrite-types';

export const makeRequest = (
  requestInfo: RequestInfo,
  requestInit: RequestInit = {},
): AppwriteReq => {
  let url: URL;
  try {
    url = new URL(requestInfo.toString());
  } catch (error) {
    const err = new Error('URL Parse Error - Did you pass an absolute URL?');
    // @ts-ignore
    err.cause = error;
    throw err;
  }
  const method = (requestInit.method || 'GET').toUpperCase();

  const headers: Record<string, string> = {};
  for (const [key, value] of Object.entries(requestInit.headers || {})) {
    headers[key.toLowerCase()] = value.toString();
  }

  const query: Record<string, string> = {};
  url.searchParams.forEach((value, key) => {
    query[key] = value;
  });

  const scheme = url.protocol.replace(':', '');

  const result = {
    bodyRaw: requestInit.body ? requestInit.body.toString() : '',
    body:
      headers['content-type'] === 'application/json'
        ? JSON.parse(requestInit.body?.toString() ?? '{}')
        : requestInit.body,
    headers,
    method,
    host: url.hostname,
    scheme,
    query,
    queryString: url.search,
    port: url.port || (scheme === 'https' ? 443 : 80),
    url: url.href,
    path: url.pathname,
  };

  // Ensure people are aware it could be either one
  if (Math.random() > 0.5)
    result.port =
      typeof result.port === 'string'
        ? parseInt(result.port)
        : result.port.toString();

  return AppwriteReq.parse(result);
};
export default makeRequest;
