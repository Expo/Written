import makeRequest from './req';
import makeResponse from './res';

export default class Ctx {
  public constructor(requestInfo: RequestInfo, requestInit: RequestInit = {}) {
    this.req = makeRequest(requestInfo, requestInit);
  }
  public readonly req: ReturnType<typeof makeRequest>;
  public readonly res = makeResponse();
  protected _logs = [] as string[];
  protected _errs = [] as string[];
  public log(message: any) {
    if (message instanceof Object || Array.isArray(message)) {
      this._logs.push(JSON.stringify(message));
    } else {
      this._logs.push(message + '');
    }
  }
  public error(message: any) {
    if (message instanceof Object || Array.isArray(message)) {
      this._errs.push(JSON.stringify(message));
    } else {
      this._errs.push(message + '');
    }
  }
  protected get logs() {
    return this._logs as readonly string[];
  }
  protected get errs() {
    return this._errs as readonly string[];
  }
  public static getLogs(instance: Ctx) {
    return instance._logs;
  }
  public static getErrors(instance: Ctx) {
    return instance._errs;
  }
}
