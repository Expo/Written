// Not testing req and res as those are tested in their own respective files
import Ctx from './ctx';

/** Ctx with public log getters */
class PlCtx extends Ctx {
  public get logs() {
    return this._logs;
  }
  public get errs() {
    return this._errs;
  }
}

describe('new Ctx()', () => {
  let ctx: PlCtx;
  beforeEach(() => {
    ctx = new PlCtx('https://example/a');
  });
  test('Ensure .req is present', () => {
    expect(ctx.req.path).toBe('/a');
  });
  test('Ensure .res is present', () => {
    expect(ctx.res.json({})?.body).toEqual('{}');
  });
  test('Ensure logging of strings works', () => {
    ctx.log('abc');
    expect(ctx.logs).toContain('abc');
  });
  test('Ensure erroring of strings works', () => {
    ctx.error('abc');
    expect(ctx.errs).toContain('abc');
  });
  test('Ensure no error->logs cross-pollution', () => {
    ctx.error('abc');
    expect(ctx.logs.includes('abc')).toBe(false);
  });
  test('Ensure no log->error cross-pollution', () => {
    ctx.log('abc');
    expect(ctx.errs.includes('abc')).toBe(false);
  });
  test.todo('Add code for validating logs of non-strings');
  test.todo('Add code for validating errors of non-strings');
});
