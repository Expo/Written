# @written/cors-setup

## 0.3.18

### Patch Changes

- a748109: Update Deps
- Updated dependencies [a748109]
  - @written/appwrite-types@0.2.17

## 0.3.17

### Patch Changes

- 51d9506: Upgrade Dependencies
- Updated dependencies [51d9506]
  - @written/appwrite-types@0.2.16

## 0.3.16

### Patch Changes

- d1ea241: Update Deps
- Updated dependencies [d1ea241]
  - @written/appwrite-types@0.2.15

## 0.3.15

### Patch Changes

- Updated dependencies [1df6d84]
  - @written/appwrite-types@0.2.14

## 0.3.14

### Patch Changes

- 4fa1758: Upgrade dependencise
- a0c9930: Upgrade Dependencies
- Updated dependencies [034c27f]
- Updated dependencies [4fa1758]
- Updated dependencies [39ce77a]
- Updated dependencies [a0c9930]
  - @written/appwrite-types@0.2.13

## 0.3.13

### Patch Changes

- Updated dependencies [816cec3]
  - @written/appwrite-types@0.2.12

## 0.3.12

### Patch Changes

- 8acd2e0: Update License Year
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format
- Updated dependencies [8acd2e0]
- Updated dependencies [bee1c23]
- Updated dependencies [de25848]
  - @written/appwrite-types@0.2.11

## 0.3.11

### Patch Changes

- 40f46fa: Upgrade Dependencies
- Updated dependencies [40f46fa]
  - @written/appwrite-types@0.2.10

## 0.3.10

### Patch Changes

- 4a5dd4d: bump dependencies
- Updated dependencies [4a5dd4d]
  - @written/appwrite-types@0.2.9

## 0.3.9

### Patch Changes

- rebuild everything due to a filesystem error during the last build
- Updated dependencies
  - @written/appwrite-types@0.2.8

## 0.3.8

### Patch Changes

- cc4fcee: Use the ~ version range

## 0.3.7

### Patch Changes

- bump everything
- Updated dependencies
  - @written/appwrite-types@0.2.7

## 0.3.6

### Patch Changes

- d3d5f04: use lib: es6 in tsconf
- Updated dependencies [18c6dd1]
  - @written/appwrite-types@0.2.6

## 0.3.5

### Patch Changes

- 1208d0d: Strict Mode, for everything
- Updated dependencies [1208d0d]
  - @written/appwrite-types@0.2.5

## 0.3.4

### Patch Changes

- Updated dependencies [bca8b56]
  - @written/appwrite-types@0.2.4

## 0.3.3

### Patch Changes

- c09e234: Add LICENSE to npm files
- Updated dependencies [c09e234]
  - @written/appwrite-types@0.2.3

## 0.3.2

### Patch Changes

- 9af1ae7: Mark as side-effect-free
- Updated dependencies [9af1ae7]
- Updated dependencies [b7955f5]
  - @written/appwrite-types@0.2.2

## 0.3.1

### Patch Changes

- Bump Version
- Updated dependencies
  - @written/appwrite-types@0.2.1

## 0.3.0

### Minor Changes

- ad89eae: Migrate to nx

### Patch Changes

- Updated dependencies [ad89eae]
- Updated dependencies [ad89eae]
  - @written/appwrite-types@0.2.0

## 0.2.2

### Patch Changes

- e1c8c20: Use esbuild & bundle everything into a single file
  - @written/appwrite-types@0.1.6

## 0.2.1

### Patch Changes

- 8917383: Add Exports Object to Package
  - @written/appwrite-types@0.1.5

## 0.2.0

### Minor Changes

- 3cc92a2: Document cors-setup, add package

### Patch Changes

- Updated dependencies [96f55e2]
  - @written/appwrite-types@0.1.4

## 0.1.3

### Patch Changes

- d23e286: Remove unnecessary axios dependencies (idk why it was there)
- Updated dependencies [d23e286]
  - @written/appwrite-types@0.1.3

## 0.1.2

### Patch Changes

- cbe6bed: Make homepage be the specific project repo dir
- Updated dependencies [cbe6bed]
  - @written/appwrite-types@0.1.2

## 0.1.1

### Patch Changes

- 50e20ad: Initial Changeset
- Updated dependencies [50e20ad]
  - @written/appwrite-types@0.1.1
