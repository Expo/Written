import type {
  AppwriteContext,
  AppwriteDefault,
  AppwriteResponseObject,
} from '@written/appwrite-types';
import { z } from 'zod';
export const setupCors = ({
  headers,
  origin,
  methods,
  allowedHeaders,
  sentHeaders,
}: Partial<{
  headers: Record<string, string>;
  origin: string;
  methods: string[];
  allowedHeaders: string[];
  sentHeaders: Record<string, string>;
}> = {}) => {
  return {
    ...(headers ?? {}),
    'Access-Control-Allow-Origin': origin ?? '*',
    'Access-Control-Allow-Methods': (
      methods ?? [
        'GET',
        'POST',
        'PATCH',
        'OPTIONS',
        'DELETE',
        ...(sentHeaders?.['Access-Control-Request-Methods']
          ?.split(',')
          ?.map(v => v.trim()) ?? []),
      ]
    ).join(', '),
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Headers': (
      allowedHeaders ?? [
        'Upgrade-Insecure-Requests',
        'X-Debug-Speed',
        'Authorization',
        'User-Agent',
        'Content-Type',
        'DNT',
        'Accept',
        'Accept-Language',
        'Content-Language',
        'Content-Type',
        'Range',
        'Origin',
        'Referrer',
        'Connection',
        'Content-Length',
        'Sec-Fetch-Dest',
        'Sec-Fetch-Mode',
        'Sec-Fetch-Site',
        'Sec-GPC',
        'Access-Control-Request-Headers',
        'Access-Control-Request-Method',
        'Cache-Control',
        'Accept-Encoding',
        'Accept-Language',
        ...(sentHeaders?.['Access-Control-Request-Headers']
          ?.split(',')
          ?.map(v => v.trim()) ?? []),
      ]
    ).join(', '),
  };
};

export const OptionsObject = z
  .object({
    origin: z.string(),
    methods: z.array(z.string()),
    allowedHeaders: z.array(z.string()),
  })
  .partial();
export type OptionsObject = z.infer<typeof OptionsObject>;

export const wrapCorsResponse = (
  ctx: AppwriteContext,
  rs: AppwriteResponseObject,
  options: Partial<OptionsObject> = {},
) => {
  rs.headers = setupCors({
    sentHeaders: ctx.req.headers,
    origin: options.origin,
    allowedHeaders: options.allowedHeaders,
    methods: options.methods,
    headers: rs.headers,
  });
  return rs;
};

export const wrapCors = (
  func: AppwriteDefault,
  options: Partial<OptionsObject> = {},
) =>
  (ctx => {
    const rs = func(ctx);
    return rs instanceof Promise
      ? rs.then(rs => wrapCorsResponse(ctx, rs, options))
      : wrapCorsResponse(ctx, rs, options);
  }) satisfies AppwriteDefault;

export default setupCors;
