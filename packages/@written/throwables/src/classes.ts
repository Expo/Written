// Throwable Classes
import { RedirectCodes } from '@written/appwrite-types';

/** General exception for us to throw, which indicates WE threw it, and should handle it. Includes basic response info */
export class Exception extends Error {
  protected readonly defaultStatus = 500 as number;
  public code = this.defaultStatus;
  public constructor(
    public message: string = 'No message specified',
    code?: number,
  ) {
    super(message);
    this.code = code ?? this.code ?? this.defaultStatus;
  }
}

/** Client-caused Exception (4xx) */
export class ClientException extends Exception {
  protected readonly defaultStatus = 400 as number;
}

/** Server-caused Exception (5xx) */
export class ServerException extends Exception {
  protected readonly defaultStatus = 500 as number;
}

/** Simple Redirect */
export class Redirect {
  public constructor(
    public target: string,
    public code: (typeof RedirectCodes)[keyof typeof RedirectCodes] = RedirectCodes.TemporaryRedirect,
  ) {}
}
