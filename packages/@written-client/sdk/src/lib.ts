import {
  Client,
  Databases,
  Account,
  Functions,
  Storage,
  Locale,
  Avatars,
  Graphql,
  Teams,
  Models,
  ExecutionMethod,
} from 'appwrite';
import { EventEmitter, type AnyFunc } from '@3xpo/events';

export type _Internal_AWClientConstructorType<T> = new (client: Client) => T;
export type _Internal_Rec<T> = [_Internal_AWClientConstructorType<T>, T];

/** Our events */
export type Events = {};

/**
 * Written SDK
 * @example ```ts
 * import SDK from '@written-client/sdk';
 * const sdk = new SDK()
 *                          .endpoint('https://example.co/v1')
 *                          .project('example-proj-id');
 * ```
 */
export class SDK<
  AdditionalEvents extends Record<string, AnyFunc> = {},
> extends EventEmitter<AdditionalEvents> {
  /** The internal Appwrite Client */
  public readonly client = new Client();

  /** If we should throw a warning on invalid execution methods. */
  public willCheckExecutionMethod = true;

  /**
   * Calls your function, returning the execution by itself
   * @param id The function's ID
   * @param route The path to call
   * @param info Additional Request Info
   */
  public _rawFetchFunction(
    id: string,
    route: string = '/',
    info: Partial<RequestInit> = {
      method: 'GET',
    },
  ) {
    const headers = {} as Record<string, string>;
    if (Array.isArray(info.headers))
      info.headers.forEach(([k, v]) => (headers[k] = v));
    else if (info.headers instanceof Headers)
      info.headers.forEach((v, k) => (headers[k] = v));
    else if (info.headers)
      Object.entries(info.headers).forEach(([k, v]) => (headers[k] = v));

    if (!id.startsWith('/'))
      throw new Error('ID does not start with a leading /, it is malformed');

    if (
      this.willCheckExecutionMethod &&
      !ExecutionMethod[info.method as ExecutionMethod]
    )
      console.warn(
        new Error(
          `Method ${info.method} is not a valid execution method. It must be one of ${Object.keys(
            ExecutionMethod,
          ).map(
            (v, i, a) =>
              `${v}${i === a.length - 1 ? '' : i === a.length - 2 ? ' or ' : ', '}`,
          )}. Set SDK.willCheckExecutionMethod to false to disable this warning.`,
        ),
      );

    return this.functions.createExecution(
      id,
      (info.body as any) ?? undefined,
      false,
      route,
      info.method as ExecutionMethod,
      headers,
    );
  }
  /**
   * Calls your function
   * @param id The function's ID
   * @param route The path to call
   * @param info Additional Request Info
   */
  public async fetchFunction(
    id: string,
    route?: string,
    info?: Partial<RequestInit>,
  ) {
    const execution = await this._rawFetchFunction(id, route, info);
    const res = new Response(execution.responseBody, {
      headers: execution.responseHeaders?.map(
        v => [v.name, v.value] as const,
      ) as [string, string][] | undefined,
      status: execution.responseStatusCode,
    }) as Response & {
      /** The internal appwrite execution object */
      execution: Models.Execution;
    };
    res.execution = execution;
    return res;
  }

  // #region Appwrite Class Getters
  private _objects = [] as _Internal_Rec<any>[];
  /** Creates an instance of the arg (a class) called with this.client - small utility function. */
  protected _<T>(constructor: _Internal_AWClientConstructorType<T>): T {
    let j = this._objects.find(v => v[0] === constructor)?.[1];
    if (!j) {
      j = new constructor(this.client);
      this._objects.push([constructor, j]);
    }
    return j;
  }
  /** If one doesn't already exist, creates an instance of {@link Databases}, returning the new instance if created - otherwise returning the existing one. */
  public get databases() {
    return this._(Databases);
  }
  /** If one doesn't already exist, creates an instance of {@link Account}, returning the new instance if created - otherwise returning the existing one. */
  public get account() {
    return this._(Account);
  }
  /** If one doesn't already exist, creates an instance of {@link Functions}, returning the new instance if created - otherwise returning the existing one. */
  public get functions() {
    return this._(Functions);
  }
  /** If one doesn't already exist, creates an instance of {@link Storage}, returning the new instance if created - otherwise returning the existing one. */
  public get storage() {
    return this._(Storage);
  }
  /** If one doesn't already exist, creates an instance of {@link Locale}, returning the new instance if created - otherwise returning the existing one. */
  public get locale() {
    return this._(Locale);
  }
  /** If one doesn't already exist, creates an instance of {@link Avatars}, returning the new instance if created - otherwise returning the existing one. */
  public get avatars() {
    return this._(Avatars);
  }
  /** If one doesn't already exist, creates an instance of {@link Graphql}, returning the new instance if created - otherwise returning the existing one. */
  public get graphql() {
    return this._(Graphql);
  }
  /** If one doesn't already exist, creates an instance of {@link Teams}, returning the new instance if created - otherwise returning the existing one. */
  public get teams() {
    return this._(Teams);
  }
  // #endregion
  // #region .endpoint()
  // #region endpoint getter/setter
  protected getEndpoint() {
    return this.client.config.endpoint;
  }
  protected setEndpoint(endpoint: string) {
    this.client.setEndpoint(endpoint);
  }
  // #endregion
  // #region .endpoint() implementation
  /**
   * Sets the appwrite endpoint to {@link newEndpoint}
   * @param newEndpoint The new appwrite endpoint to use
   * @returns {this} The current instance of SDK.
   * @example ```ts
   * const sdk = new SDK().endpoint('https://api.example.co/v1'); // <== the currently described overload
   * console.log(sdk.endpoint()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  public endpoint(newEndpoint: string): this;
  /**
   * Gets the current endpoint.
   * Pass an argument to set the endpoint.
   * @returns {string} the current endpoint
   * @example ```ts
   * const sdk = new SDK().endpoint('https://api.example.co/v1');
   * console.log(sdk.endpoint()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  // @ts-ignore
  public endpoint(): string;
  public endpoint(newEndpoint?: string | undefined | null) {
    if (newEndpoint) {
      this.setEndpoint(newEndpoint);
      return this;
    } else return this.endpoint;
  }
  // #endregion
  // #endregion
  // #region .realtimeEndpoint()
  // #region endpoint getter/setter
  protected getRealtimeEndpoint() {
    return this.client.config.endpointRealtime;
  }
  protected setRealtimeEndpoint(endpoint: string) {
    this.client.setEndpointRealtime(endpoint);
  }
  // #endregion
  // #region .realtimeEndpoint() implementation
  /**
   * Sets the appwrite {@link https://appwrite.io/docs/apis/realtime realtime} endpoint to {@link newEndpoint}
   * @param newEndpoint The new appwrite endpoint to use
   * @returns {this} The current instance of SDK.
   * @example ```ts
   * const sdk = new SDK().realtimeEndpoint('https://api.example.co/v1'); // <== the currently described overload
   * console.log(sdk.realtimeEndpoint()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  public realtimeEndpoint(newEndpoint: string): this;
  /**
   * Gets the current endpoint.
   * Pass an argument to set the endpoint.
   * @returns {string} the current endpoint
   * @example ```ts
   * const sdk = new SDK().realtimeEndpoint('https://api.example.co/v1');
   * console.log(sdk.realtimeEndpoint()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  // @ts-ignore
  public realtimeEndpoint(): string;
  public realtimeEndpoint(newEndpoint?: string | undefined | null) {
    if (newEndpoint) {
      this.setRealtimeEndpoint(newEndpoint);
      return this;
    } else return this.endpoint;
  }
  // #endregion
  // #endregion
  // #region .project() implementation
  protected getProject() {
    return this.client.config.project;
  }
  protected setProject(endpoint: string) {
    this.client.setProject(endpoint);
  }
  /**
   * Sets the appwrite {@link https://appwrite.io/docs/apis/realtime realtime} endpoint to {@link newEndpoint}
   * @param newEndpoint The new appwrite endpoint to use
   * @returns {this} The current instance of SDK.
   * @example ```ts
   * const sdk = new SDK().project('https://api.example.co/v1'); // <== the currently described overload
   * console.log(sdk.project()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  public project(newEndpoint: string): this;
  /**
   * Gets the current endpoint.
   * Pass an argument to set the endpoint.
   * @returns {string} the current endpoint
   * @example ```ts
   * const sdk = new SDK().project('https://api.example.co/v1');
   * console.log(sdk.project()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  // @ts-ignore
  public project(): string;
  public project(newEndpoint?: string | undefined | null) {
    if (newEndpoint) {
      this.setProject(newEndpoint);
      return this;
    } else return this.endpoint;
  }
  // #endregion
  // #region .JWT()
  // #region endpoint getter/setter
  protected getJWT() {
    return this.client.config.jwt;
  }
  protected setJWT(endpoint: string) {
    this.client.setJWT(endpoint);
  }
  // #endregion
  // #region .JWT() implementation
  /**
   * Sets the appwrite JWT to {@link jwt}
   * @param jwt The new appwrite endpoint to use
   * @returns {this} The current instance of SDK.
   * @example ```ts
   * const sdk = new SDK().JWT('https://api.example.co/v1'); // <== the currently described overload
   * console.log(sdk.JWT()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  public JWT(jwt: string): this;
  /**
   * Gets the current JWT.
   * Pass an argument to set the JWT.
   * @returns {string} the current JWT
   * @example ```ts
   * const sdk = new SDK().JWT('https://api.example.co/v1');
   * console.log(sdk.JWT()); // ==> returns 'https://api.example.co/v1'
   * ```
   */
  // @ts-ignore
  public JWT(): string;
  public JWT(jwt?: string | undefined | null) {
    if (jwt) {
      this.setJWT(jwt);
      return this;
    } else return this.endpoint;
  }
  // #endregion
  // #endregion
}
export default SDK;
