export default {
  $schema: './node_modules/nx/schemas/nx-schema.json',
  extends: 'nx/presets/npm.json',
  affected: {
    defaultBase: 'senpai',
  },
  targetDefaults: {
    build: {
      cache: true,
      dependsOn: ['^build'],
    },
    lint: {
      cache: true,
    },
    e2e: {
      cache: true,
    },
  },
  neverConnectToCloud: true,
};
