import { optFunc } from '../util/optFunc';

export const generateTsConf = optFunc(
  opt => ({
    $schema: 'https://json.schemastore.org/tsconfig.json',
    compilerOptions: {
      rootDir: opt.rootDir,
      outDir: opt.outDir,
      declaration: true,
      sourceMap: true,
      declarationMap: true,
      esModuleInterop: true,
      strictNullChecks: true,
      strict: true,
      skipLibCheck: true,
      resolveJsonModule: true,
      module: 'ES6',
      moduleResolution: 'Bundler',
    },
    include: ['src/**/*', 'src/*'],
    exclude: ['dist/**/*', 'dist/*', 'src/*.test.*s', 'src/**/*.test.*s'],
  }),
  {
    rootDir: 'src',
    outDir: 'dist',
  },
);

export default generateTsConf;
