/** Wraps a function to allow optional arguments */
export const optFunc =
  <
    Opt extends Record<string, any>,
    RT extends any,
    PartialOpt extends Partial<Opt> = Partial<Opt>,
  >(
    fn: (opt: Opt) => RT,
    defaultOptions: Opt,
  ) =>
  (opt?: PartialOpt) =>
    fn({
      ...defaultOptions,
      ...opt,
    });
export default optFunc;
