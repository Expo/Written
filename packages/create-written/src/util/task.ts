import { spinner } from '@expove/clack__prompts';
import outro from './outro';
import consola from 'consola';

export type Task = {
  /**
   * Task title
   */
  title: string;
  /**
   * Task function
   */
  task: (
    message: (string: string) => void,
  ) => string | Promise<string> | void | Promise<void>;

  /**
   * If enabled === false the task will be skipped
   */
  enabled?: boolean;
};

/**
 * Define a group of tasks to be executed
 */
export const tasks = async (tasks: Task[]) => {
  for (const task of tasks) {
    if (task.enabled === false) continue;

    const s = spinner();
    s.start(task.title);
    try {
      const result = await task.task(s.message);
      s.stop(result || task.title);
    } catch (error) {
      s.stop(task.title + ': Task Failed');
      outro(`Task Failed: ${task.title}`, true);
      consola.error(error);
      process.exit(1);
    }
  }
};
