import {
  text,
  confirm,
  outro as clackOutro,
  intro,
} from '@expove/clack__prompts';
import optFunc from '../util/optFunc';
import { notCancel } from '../util/notCancel';
import path from 'path';
import fse, { writeFile } from '@3xpo/fs-extra';
import { execSync } from 'child_process';
import launch from '@factored/launch-editor';
import consola from 'consola';
import outro from '../util/outro';
import { setupBaseMonorepo } from '@written/base-monorepo-setup';
import { rootPkg } from '../files/root-pkg';
import rootDependencies from '../files/root-dependencies';
import { tasks as tasksFunc } from '../util/task';
import packageJson from '../../package.json';
import { create as createSvelte } from 'create-svelte';
import fs from 'fs/promises';
import baseProject from '../files/base-project';
import generateTsConf from '../files/tsconf';

const { ensureDirSync, existsSync, readdirSync, writeFileSync } = fse;

const homedir = process.env.HOME ?? process.env.USERPROFILE ?? '/';

/**
 * create-written/project
 * Creates a new written project
 */
export const createWrittenProject = optFunc(
  async opt => {
    opt.useFunctions = opt.useFunctions ?? true;
    // #region Ensure Options
    const { tasks } = opt;
    const ensureInteractive = () => {
      if (!opt.interactive)
        throw new Error(
          'Unable to request interaction if opt.interact=false. Make sure to specify *all* null-ish values.',
        );
    };
    const _cwdIsEmpty = readdirSync(path.resolve(process.cwd())).length === 0;
    // #region Project Name
    opt.name =
      opt.name ??
      ensureInteractive() ??
      notCancel(
        await text({
          message: `What's your project's name?`,
          validate: v => {
            v = (v ?? '').trim();
            if (!v) return 'Please enter a value.';
          },
        }),
      );
    // #endregion
    // #region Get the installation directory
    opt.dir =
      opt.dir ??
      ensureInteractive() ??
      path.resolve(
        notCancel(
          await text({
            message: 'Where do you want to create the new project?',
            initialValue: process.env.IS_DOING_CREATE_DEV
              ? '/tmp/test' + Date.now()
              : undefined,
            placeholder:
              opt.allowCwd && (_cwdIsEmpty || opt.allowNonEmpty)
                ? 'Leave blank for the current directory'
                : undefined,
            defaultValue:
              opt.allowCwd && (_cwdIsEmpty || opt.allowNonEmpty)
                ? process.cwd()
                : undefined,
            validate: val => {
              if (!val && !(opt.allowCwd && (_cwdIsEmpty || opt.allowNonEmpty)))
                return 'Please select a directory.';
              val = (val ?? process.cwd()).trim();
              if (val.startsWith('~')) val = val.replace('~', homedir);
              if (path.resolve(val, '..') === path.resolve(val))
                return 'Cannot install to the root directory of your system.';
              if (path.resolve(val) === path.resolve(homedir))
                return 'Cannot install to your home directory. Pick a subdirectory.';
              if (
                !opt.allowCwd &&
                path.resolve(val) === path.resolve(process.cwd())
              )
                return 'CWD is not allowed given the current options.';
              if (
                !opt.allowNonEmpty &&
                existsSync(val) &&
                readdirSync(path.resolve(val)).length > 0
              )
                return 'The selected directory has files! Please pick an empty directory!';
            },
          }),
        ),
      );
    // #endregion
    // #region Check if we should use git
    opt.git = existsSync(path.join(opt.dir, '.git'))
      ? false
      : opt.git ??
        ensureInteractive() ??
        notCancel(
          await confirm({
            message: 'Do you want to initialize a Git repository?',
            initialValue: process.env.IS_DOING_CREATE_DEV ? false : true,
          }),
        );
    // #endregion
    // #endregion
    // #region Installation
    // TODO: Check if the user is signed in - see https://github.com/appwrite/appwrite/issues/7492.

    // Do the base setup
    const workspacePaths = ['packages/*'];
    if (opt.useFunctions) workspacePaths.push('packages/appwrite/functions/*');
    await setupBaseMonorepo({
      dir: opt.dir,
      git: opt.git,
      packageJSON: rootPkg({
        name: opt.name,
      }),
      rootDependencies,
      workspacePaths,
      tasks,
    });

    // Setup the Appwrite Project
    if (opt.appwriteInteractive) {
      clackOutro(`Launching Appwrite Setup - We'll be right back.`);
      ensureDirSync(opt.dir + '/packages/appwrite');
      execSync('appwrite init project', {
        stdio: 'inherit',
        cwd: opt.dir + '/packages/appwrite',
      });
      writeFileSync(
        opt.dir + '/packages/appwrite/project.json',
        JSON.stringify(baseProject, null, 2),
      );
      writeFileSync(
        opt.dir + '/packages/appwrite/package.json',
        JSON.stringify(
          {
            name: 'appwrite',
            scripts: {
              dev: 'nx run-many --parallel=1 -t aw-deploy',
            },
            private: true,
          },
          null,
          2,
        ),
      );
      console.log('');
      intro(`${packageJson.name} v${packageJson.version} - Welcome Back!`);
    }

    // Setup Svelte, Prettier, TS
    await tasks([
      {
        title: 'Setting up Svelte',
        async task() {
          await new Promise<never>(rs =>
            setTimeout(() => rs(void 0 as never), 100),
          );
          await createSvelte(opt.dir + '/packages/svelte', {
            name: 'svelte-project',
            template: 'skeleton',
            types: 'typescript',
            prettier: false,
            eslint: false,
            playwright: false,
            vitest: false,
          });
          execSync('pnpm i', {
            cwd: opt.dir + '/packages/svelte',
          });
          await writeFile(
            opt.dir + '/packages/svelte/project.json',
            JSON.stringify(baseProject, null, 2),
          );
          return 'Finished Svelte Setup!';
        },
        enabled: opt.shouldSetupSvelte ?? true,
      },
      {
        title: 'Setting up Prettier',
        async task() {
          await fs.writeFile(
            opt.dir + '/.prettierrc',
            await fetch(
              'https://codeberg.org/Expo/prettier/raw/branch/senpai/.prettierrc-svelte',
            ).then(v => v.text()),
          );
          return 'Finished Prettier Setup!';
        },
      },
      {
        title: 'Setting up TS',
        async task() {
          await fs.writeFile(
            opt.dir + '/tsconfig.json',
            JSON.stringify(
              generateTsConf({
                rootDir: undefined,
                outDir: undefined,
              }),
            ),
          );
          return 'Finished TypeScript Setup!';
        },
      },
    ]);

    // #endregion
    // #region Post-Setup Editor Stuff
    if (
      opt.openEditor ??
      (opt.interactive
        ? notCancel(
            await confirm({
              message:
                'Do you want to open the resulting project in your editor?',
              initialValue: true,
            }),
          )
        : false)
    ) {
      let launched = false;
      try {
        if (process.env.EDITOR) {
          execSync(process.env.EDITOR + ' ' + JSON.stringify(opt.dir), {
            stdio: 'inherit',
            cwd: opt.dir,
          });
          launched = true;
        }
      } catch (_) {}
      if (!launched)
        launch(opt.dir, err => {
          outro('Could not open editor');
          consola.error('Error launching editor:', err);
        });
    }
    // #endregion
    outro('Done!');
    return opt;
  },
  {
    /** When false, skips all interactions with values. */
    interactive: true,
    /** When false, skips running `appwrite init project` - an interactive command. You should only set this to false if you need a truly fully interactivity-less experience */
    appwriteInteractive: true,
    /** The name of the project */
    name: null as null | string,
    /** The directory to create the project in */
    dir: null as null | string,
    /** Allows creating the project in the current directory */
    allowCwd: true,
    /** Allows creating the project in a non-empty directory */
    allowNonEmpty: !!process.env.ALLOW_NON_EMPTY_DIR,
    /** Whether we should initialize a new git repo */
    git: null as null | boolean,
    /** Whether to open the default editor when done */
    openEditor: null as null | boolean,
    /** Whether we should use Appwrite Functions or not */
    useFunctions: null as null | boolean,
    /** Function used to run tasks & shit */
    tasks: tasksFunc,
    /** If we should setup svelte - set to false to use a custom frontend */
    shouldSetupSvelte: true,
  },
);

export default createWrittenProject;
