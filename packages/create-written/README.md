<div align="center">

![Written Logo](https://codeberg.org/Written/Written/raw/branch/senpai/ico/written.png)

# create-written

Creates a new [Written](https://codeberg.org/Written) project.

[![Codeberg](https://img.shields.io/badge/Codeberg-Written-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/packages/create-written)
[![License](https://img.shields.io/badge/License-MIT-724773.svg?style=flat-square)](https://codeberg.org/Written/Written/src/branch/senpai/LICENSE)

</div>

## Usage

```sh
pnpm create written
```

Note: This package requires you to have an existing Appwrite instance to use.<br/>
If you don't have an Appwrite instance, you can self-host one; see [appwrite.io](https://appwrite.io) for more information.

TODO: Add further usage instructions.
