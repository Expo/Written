# create-written

## 0.2.11

### Patch Changes

- a748109: Update Deps
- Updated dependencies [a748109]
  - @written/base-monorepo-setup@0.1.13

## 0.2.10

### Patch Changes

- 51d9506: Upgrade Dependencies
- Updated dependencies [51d9506]
  - @written/base-monorepo-setup@0.1.12

## 0.2.9

### Patch Changes

- d1ea241: Update Deps
- Updated dependencies [d1ea241]
  - @written/base-monorepo-setup@0.1.11

## 0.2.8

### Patch Changes

- 4fa1758: Upgrade dependencise
- a0c9930: Upgrade Dependencies
- Updated dependencies [4fa1758]
- Updated dependencies [a0c9930]
  - @written/base-monorepo-setup@0.1.10

## 0.2.7

### Patch Changes

- 3872f26: Add LICENSE files
- bee1c23: Move repos to Written/Written
- de25848: Consistent, Clean README header format
- Updated dependencies [3872f26]
- Updated dependencies [bee1c23]
- Updated dependencies [de25848]
  - @written/base-monorepo-setup@0.1.9

## 0.2.6

### Patch Changes

- 40f46fa: Upgrade Dependencies
- Updated dependencies [40f46fa]
  - @written/base-monorepo-setup@0.1.8

## 0.2.5

### Patch Changes

- 8893021: default useFunctions to true
- 4a5dd4d: bump dependencies
- Updated dependencies [4a5dd4d]
  - @written/base-monorepo-setup@0.1.7

## 0.2.4

### Patch Changes

- 7869ef7: use a shebang

## 0.2.3

### Patch Changes

- rebuild everything due to a filesystem error during the last build
- Updated dependencies
  - @written/base-monorepo-setup@0.1.6

## 0.2.2

### Patch Changes

- c95cd7e: wait a moment so the message goes through

## 0.2.1

### Patch Changes

- 456937f: call pnpm i after createSvelte
- 9b00c93: install ts-node, svelte and typescript in the root

## 0.2.0

### Minor Changes

- c9cde54: modify deploy script; we now have deploy and aw-deploy

### Patch Changes

- d255648: modify root package a bit

## 0.1.9

### Patch Changes

- cc4fcee: Use the ~ version range

## 0.1.8

### Patch Changes

- e4e1cb2: slug package name

## 0.1.7

### Patch Changes

- fix update checker
- Updated dependencies [f14b10d]
  - @written/base-monorepo-setup@0.1.5

## 0.1.6

### Patch Changes

- 3e12f61: export the internal modules
- bump everything
- Updated dependencies
  - @written/base-monorepo-setup@0.1.4

## 0.1.5

### Patch Changes

- dd14f4c: use @3xpo/fs-extra
- bd7aa58: Upgrade Dependencies
- ea07dea: Vendor source-map-support
- ce3a20a: dont bundle create-svelte
- Updated dependencies [dd14f4c]
- Updated dependencies [bd7aa58]
- Updated dependencies [18c6dd1]
  - @written/base-monorepo-setup@0.1.3

## 0.1.4

### Patch Changes

- 8cb4d29: Use ESM for create

## 0.1.3

### Patch Changes

- Vendor create-svelte

## 0.1.2

### Patch Changes

- af783d9: move fs-extra to deps
- Updated dependencies [af783d9]
  - @written/base-monorepo-setup@0.1.2

## 0.1.1

### Patch Changes

- a60066a: Add a half-completed setup of everything
- 48bc017: Upgrade Dependencies
- Updated dependencies [48bc017]
  - @written/base-monorepo-setup@0.1.1
